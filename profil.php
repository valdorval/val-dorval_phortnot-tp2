<?php
include 'includes/elements/header.php';
include 'includes/functions/data/db.php';

if (isset($_POST['submit-img'])) {
     try {
          $newImg = $pdo->prepare('UPDATE profil SET id_image = ? WHERE id = ?');
          $newImg->execute([$_POST['profil'], $_SESSION['auth']['id']]);
          $successMessage = "Votre image de profil a été modifiée avec succès";
     } catch (PDOException $e) {
          echo 'Erreur : ' . $e->getMessage();
     }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit-mail'])) {
     if (validate_email($_POST["new-mail"])) {
          edit_email($_POST["new-mail"], $_SESSION['auth']['id']);
          $successMessage = "Votre adresse courriel a été modifiée avec succès";
     }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submit-user"])) {
     edit_user($_POST['new-user'], $_SESSION['auth']['id']);
     $successMessage = "Votre nom d'utilisateur a été modifié avec succès";
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submit-pass"])) {
     if ($_POST['new-pass'] === $_POST['confirm-new-pass']) {
          edit_pass(encrypt_pass($_POST["new-pass"]), $_SESSION['auth']['id']);
          $successMessage = "Votre mot de passe a été modifié avec succès";
     }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submit-phone"])) {
     if (!empty($_POST['new-phone']) && preg_match($regex, $_POST['new-phone'])) {
          edit_phone(phone_replace($_POST['new-phone']), $_SESSION['auth']['id']);
          $successMessage = "Votre numéro de téléphone a été modifié avec succès";
     }
}

?>

<?php if (isset($_SESSION['auth'])) : ?>

     <?php
     $connexion = connect_by_id($_SESSION['auth']['id']);
     $user = $connexion->fetch();
     ?>

     <section class="section-profil">
          <h2>Bonjour <?php echo $user['first_name'] ?> !</h2>
          <div class="profil">

               <?php
               if (isset($successMessage)) {
                    echo '<p class="success-msg">' . $successMessage . '</p>';
               }

               ?>

               <h3>Votre profil</h3>
               <div class="flex profil-content">
                    <div class="profil-detail">
                         <p>Prénom: <?php echo $user['first_name'] ?></p>
                         <p>Nom: <?php echo $user['last_name'] ?></p>
                         <p>Date de naissance: <?php echo $user['birth'] ?></p>
                         <p>Numéro de téléphone: <?php echo format_phone($user['phone']) ?><img src="img/edit.png" alt="editer" class="edit phone"></p>
                         <p>Courriel: <?php echo $user['email'] ?><img src="img/edit.png" alt="editer" class="edit mail"></p>
                         <p>Nom d'utilisateur: <?php echo $user['username'] ?><img src="img/edit.png" alt="editer" class="edit user"></p>
                         <p>Modifier le mot de passe<img src="img/edit.png" alt="editer" class="edit pass"></p>
                    </div>

                    <div class="image-profil">
                         <img src="img/profil/perso<?php echo $user['id_image'] ?>.jpg" alt="image profil" id="chx-img">
                         
                         <div class="img-choix">
                              <div class="close-btn"></div>
                              <form action=" <?php echo htmlentities($_SERVER['PHP_SELF']) ?> " method="POST">
                                   <h3>Choix de la nouvelle image de profil:</h3>

                                   <?php
                                   try {
                                        $chxImg = $pdo->query('SELECT * FROM image_profil');
                                        foreach ($chxImg as $image) {
                                             echo '<label>';
                                             echo '<input type="radio" class="choix" name="profil" value="' . $image['id']  . '">';
                                             echo '<img src="img/profil/perso' . $image['id']  . '.jpg" alt="choix d\'image">';
                                             echo '</label>';
                                        }
                                   } catch (PDOException $e) {
                                        echo 'Erreur : ' . $e->getMessage();
                                   }
                                   ?>

                                   </select><br>
                                   <button type="submit" name="submit-img" class="submit choix">Choisir l'image</button>
                              </form>
                         </div>
                    </div>

                    <div class="edit-phone">
                         <div class="close-btn"></div>
                         <form action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" method="POST">
                              <h3>Modification de votre numéro de téléphone:</h3>
                              <div class="conteneur">
                                   <span class="error-message"></span>
                                   <div>
                                        <label>Nouveau numéro de téléphone:</label>
                                        <input type="text" name="new-phone" class="input" id="new-phone">
                                   </div>
                                   <button type="submit" name="submit-phone" class="submit choix" id="submit-phone">Envoyer</button>
                              </div>
                         </form>
                    </div>

                    <div class="edit-mail">
                         <div class="close-btn"></div>
                         <form action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" method="POST">
                              <h3>Modification de votre adresse courriel:</h3>
                              <div class="conteneur">
                                   <div>
                                        <span class="error-message"></span>
                                        <p>Adresse courriel actuelle:
                                             <?php
                                             echo $user['email'];
                                             ?>
                                        </p>
                                   </div>

                                   <div>
                                        <label>Nouvelle adresse courriel:</label>
                                        <input type="text" name="new-mail" class="input" id="new-mail">
                                   </div>
                              </div>
                              <button type="submit" name="submit-mail" class="submit choix" id="submit-mail">Envoyer</button>
                         </form>
                    </div>

                    <div class="edit-user">
                         <div class="close-btn"></div>
                         <form action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" method="POST">
                              <h3>Modification de votre nom d'utilisateur:</h3>
                              <div class="conteneur">
                                   <span class="error-message"></span>
                                   <div>
                                        <label>Nouveau nom d'utilisateur:</label>
                                        <input type="text" name="new-user" class="input" id="new-user">
                                   </div>
                                   <button type="submit" name="submit-user" class="submit choix" id="submit-user">Envoyer</button>
                              </div>
                         </form>
                    </div>

                    <div class="edit-pass">
                         <div class="close-btn"></div>
                         <form action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" method="POST">
                              <h3>Modification de votre mot de passe:</h3>
                              <div class="conteneur">
                                   <span class="error-message"></span>
                                   <div>
                                        <label>Nouveau mot de passe:</label>
                                        <input type="password" name="new-pass" class="input" id="new-pass">
                                   </div>
                                   <div>
                                        <label>Confirmation du nouveau mot de passe:</label>
                                        <input type="password" name="confirm-new-pass" class="input" id="confirm-new-pass">
                                   </div>
                                   <button type="submit" name="submit-pass" class="submit choix" id="submit-pass">Envoyer</button>
                              </div>
                         </form>
                    </div>

               </div>
          </div>
     </section>

     <section class="section-profil">
          <div class="profil">
               <h3>Détails de votre participation au grand tournoi Phortnot</h3>
               <div class="flex profil-content">
                    <div class="profil-detail">
                         <p><strong>Voici les détails de votre progression: </strong></p>
                         <p>Nombre de victoires: <?php echo $user['first_name'] ?></p>
                         <p>Nombre de défaites: <?php echo $user['last_name'] ?></p>
                         <p>Grand champion: <?php echo $user['birth'] ?></p>
                         <p>Date de mort: <?php echo format_phone($user['phone']) ?></p>
                    </div>
                    <div class="detail-tournoi">
                         <p>Votre courage et votre volonté seront mis à l'épreuve. </p>
                         <p>Vous lutterez, vous périrez, vous vaincrez! À la sueur de votre front, vous devrez gravir les échelons pour devenir l'ultime gagnant du grand tournoi Phortnot.</p>
                         <p><strong>Bonne chance!</strong></p>
                    </div>
               </div>
          </div>
     </section>

<?php else :
     header('Location: signin.php');
     exit();
endif; ?>

</main>

<?php
require 'includes/elements/footer.php';
?>
$(function () {
        var modeleMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var modelePhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

        function validPhone() {
                var phone = $('#new-phone').val();
                if (phone == "" || !modelePhone.test(phone)) {
                        $('.edit-phone .error-message').show();
                        $('.edit-phone .error-message').text('Numéro de téléphone non valide');
                        return false;
                }
                return true;
        }

        $('#submit-phone').click(function (e) {
                if (!validPhone()) {
                        e.preventDefault();
                }
        })

        function validMail() {
                var mail = $('#new-mail').val();
                if (!modeleMail.test(mail)) {
                        $('.edit-mail .error-message').show();
                        $('.edit-mail .error-message').text('Adresse courriel non valide');
                        return false;
                }
                return true;
        }

        $('#submit-mail').click(function (e) {
                if (!validMail()) {
                        e.preventDefault();
                }
        })

        function validUser() {
                var user = $('#new-user').val();
                if (user == "") {
                        $('.edit-user .error-message').show();
                        $('.edit-user .error-message').text('Le nom d\'utilisateur ne peut pas être vide');
                        return false;
                }
                return true;
        }

        $('#submit-user').click(function (e) {
                if (!validUser()) {
                        e.preventDefault();
                }
        })

        function validPass() {
                var newPass = $('#new-pass').val();
                var confirmPass = $('#confirm-new-pass').val();
                if (newPass != confirmPass || (newPass == "" || confirmPass == "")) {
                        $('.edit-pass .error-message').show();
                        $('.edit-pass .error-message').text('Mot de passe non valide');
                        return false;
                }
                return true;
        }

        $('#submit-pass').click(function (e) {
                if (!validPass()) {
                        e.preventDefault();
                }
        })


        $('.sidebarBtn').click(function () {
                $('.sidebar').toggleClass('activeBar');
                $('.sidebarBtn').toggleClass('toggle');
        });

        $('.nav-menu li a').mouseover(function () {
                $(this).stop().animate({ backgroundPosition: '0px -450px' }, 500);
        });

        $('.nav-menu li a').mouseout(function () {
                $(this).stop().animate({ backgroundPosition: '0px 0px' }, 500);
        });

        $('#btn').click(function () {
                $('#mention').toggle();
                $('.el-left').show();
                $('.el-right').show();
                $('.mention_detail').toggle();
        });

        function toggleItem(item, modify) {
                item.click(function () {
                        modify.toggle();
                })
        }

        toggleItem($('#chx-img'), $('.img-choix'));
        toggleItem($('.mail'), $('.edit-mail'));
        toggleItem($('.user'), $('.edit-user'));
        toggleItem($('.pass'), $('.edit-pass'));
        toggleItem($('.phone'), $('.edit-phone'));
        toggleItem($('.img-choix .close-btn'), $('.img-choix'));
        toggleItem($('.edit-mail .close-btn'), $('.edit-mail'));
        toggleItem($('.edit-user .close-btn'), $('.edit-user'));
        toggleItem($('.edit-pass .close-btn'), $('.edit-pass'));
        toggleItem($('.edit-phone .close-btn'), $('.edit-phone'));

});

$('.firework').fireworks({
        sound: false, // sound effect
        opacity: 0.4,
        width: '150%',
        height: '150%'
});


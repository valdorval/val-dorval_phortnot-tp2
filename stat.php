<?php
require 'includes/elements/header.php';
require 'includes/functions/data/db.php';
?>

<section class="statistique conteneur">
     <h2>Statistiques des participants</h2>
     <div class="stat-content"></div>
     <?php
     $stat = tournoi_info();
     $i = 0;
     foreach ($stat as $type) {
          echo '<div class="flex stat-line">';
          echo '<div class="images">';
          echo '<div class="color"><img src="img/profil/perso' . $type['id_image'] . '.jpg" alt="image profil"></div>';
          echo '</div>';
          echo '<div class="details" id="perso' . $i . '">';
          echo '<h3>' . $type['username'] . '</h3>';
          echo '<div class="stat-pos">';
          echo '<p>Victoire: ' . ($type['win'] == null ? 0 : $type['win']) . '</p>';
          echo '<p>Défaite: ' . ($type['lose'] == null ? 0 : $type['lose']) . '</p>';
          echo '<p>Champion: ' .  ($type['champion'] == 0 ? 'non' : 'oui') . '</p>';
          if ($type['champion'] == 1) {
               echo '<p>Date de mort: ' . $type['death'] . '</p>';
          }
          echo '</div>';
          echo '</div>';
          echo '</div>';
          $i++;
     } ?>


</section>

</main>

<?php
require 'includes/elements/footer.php';
?>
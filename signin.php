<?php
include 'includes/elements/header.php';
include 'includes/functions/data/db.php';

$connexion = null;
$success = null;
$error_log = null;

if ($errors) {
     foreach ($errors as $error) {
          if ($error === '') {
               $connexion = true;
          } else {
               $connexion = false;
          }
     }
}

if ($connexion) {
     try {
          $req = $pdo->prepare('INSERT INTO profil (first_name, last_name, email, birth, phone, username, pwd)
          VALUES (?, ?, ?, ?, ?, ?, ?)');
          $password = encrypt_pass($_POST['password']);
          $req->execute([$_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['birth'], phone_replace($_POST['phone']), $_POST['user_name'], $password]);
          $_SESSION['success'] = 'L\'inscription a bien été effectuée';
          header('Location:signin.php');

          // $user = $pdo->lastInsertId();
     } catch (PDOException $e) {
          echo 'Erreur : ' . $e->getMessage();
     }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"  && isset($_POST['loginBtn'])) {
     $conn = $_POST['username'];

     try {
          $requete = $pdo->prepare('SELECT * FROM profil WHERE username = ?');
          $requete->execute([$conn]);
          $userInfo = $requete->fetch();

          if ($conn === $userInfo['username']) {
               $pass = $_POST['pwd'];
               if (password_verify($pass, $userInfo['pwd'])) {
                    $_SESSION['auth'] = $userInfo;
                    $success = 'Connexion réussie';
                    header('Location:profil.php');
               }
          } else {
               $error_log = 'Nom d\'utilisateur ou mot de passe incorrecte';
          }
     } catch (PDOException $e) {
          echo 'Erreur : ' . $e->getMessage();
     }
}

?>

<section class="login">
     <h2>Connexion</h2>
     <!-- <?php (!empty($error_log) ? '<p class="erreur-login">' . $error_log . '</p>' : ''); ?> -->
     <div class="flex login-form">
          <form action="" method="POST" class="flex">
               <p>
                    <input type="text" name="username" class="input" placeholder="Identifiant">
               </p>
               <p>
                    <input type="password" name="pwd" class="input" placeholder="Mot de passe">
               </p>
               <button type="submit" class="submit" name="loginBtn">Se connecter</button>
          </form>
     </div>
     <?php if (isset($error_log) && !empty($error_log)) {
          echo '<p class="erreur-login">' . $error_log . '</p>';
     } elseif (isset($_SESSION['success'])) {
          echo  '<p class="success-login">' . $_SESSION['success'] . '</p>';
     }

     ?>
</section>

<section class="flex main-form">
     <div class="separation">
          <h3>Pas encore inscrit? Créer un compte:</h3>
          <div class="triangle"></div>
     </div>

     <div class="flex">
          <div class="width100">
               <div class="inscription">
                    <div class="flex form-inscription">
                         <div class="form">
                              <form action="" method="POST">
                                   <p class="flex">
                                        <label>Prénom: </label>
                                        <input type="text" name="first_name" class="input large" value="<?= form_values('first_name'); ?>">
                                   </p>
                                   <p class="flex">
                                        <label>Nom: </label>
                                        <input type="text" name="last_name" class="input large" value="<?= form_values('last_name'); ?>">
                                   </p>
                                   <p class="flex">
                                        <label>Numéro de téléphone: </label>
                                        <input type="text" name="phone" class="input large" value="<?= form_values('phone'); ?>">
                                   </p>
                                   <p class="flex">
                                        <label>Date de naissance: </label>
                                        <input type="date" name="birth" class="input large" value="<?= form_values('birth'); ?>">
                                   </p>
                         </div>
                         <div class="form">
                              <p class="flex">
                                   <label>Adresse courriel: </label>
                                   <input type="courriel" name="email" class="input large" value="<?= form_values('email'); ?>">
                              </p>
                              <p class="flex">
                                   <label>Nom d'utilisateur: </label>
                                   <input type="text" name="user_name" class="input large" value="<?= form_values('user_name'); ?>">
                              </p>
                              <p class="flex">
                                   <label>Mot de passe: </label>
                                   <input type="password" name="password" class="input large">
                              </p>
                              <p class="flex">
                                   <label>Confirmation de mot de passe: </label>
                                   <input type="password" name="password_confirm" class="input large">
                              </p>
                              <button type="submit" class="submit form-submit" name="submit"> Envoyer </button>
                              </form>
                         </div>
                    </div>
               </div>
          </div>

          <?php
          if (isset($errors) && !empty($errors)) {
          ?>

               <div class="erreur">
                    <h3>Erreurs dans votre inscription</h3>

               <?php
               foreach ($errors as $error) {
                    echo "<div>" . $error . "</div>";
               }
          }
               ?>
               </div>
     </div>


</section>
</main>

<?php
require 'includes/elements/footer.php';
?>
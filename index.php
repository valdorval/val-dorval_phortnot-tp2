<?php
include 'includes/elements/header.php';
?>

<h1 class="main-title">Bienvenue sur le site de Phortnot!</h1>

<section class="section1">
      <div class="anim">
            <div class="anim1">
                  <div class="mage-corps"></div>
                  <div class="bras-droit"></div>
                  <div class="bras-gauche"></div>
                  <div class="roche"></div>
                  <div class="roche r1"></div>
                  <div class="roche r2"></div>
                  <div class="roche r3"></div>
                  <div class="roche r4"></div>
                  <div class="roche r5"></div>
                  <div class="roche r6"></div>
                  <div class="roche r7"></div>
                  <div class="roche r8"></div>
                  <div class="roche r9"></div>
                  <div class="roche r10"></div>
                  <div class="roche r11"></div>
                  <div class="new-roche"></div>
            </div>

            <div class="anim2">
                  <div class="vs">VS</div>
                  <div class="fade"></div>
                  <div class="war"></div>
                  <div class="mage"></div>
                  <div class="mage-arrive"></div>
                  <div class="war-arrive"></div>
            </div>

            <div class="inbetween">
                  <div class="defilement"></div>
            </div>

            <div class="anim3">
                  <div class="congrat">Félicitation au grand champion!</div>
                  <div class="fond-etoile"></div>
                  <div class="personnage"></div>
                  <div class="firework pos"></div>
            </div>
      </div>
</section>

<section>
      <div class="mention">
            <p class="mention_detail">Voir les détails</p>
            <button id="btn" class="btn-anim" type="button">Mentions</button>
            <div id="mention" class="mention_content">
                  <div class="el-left">salut</div>
                  <div class="el-right"></div>
                  <div class="container">
                        <h3>Éléments graphiques</h3>
                        <p>Chacun des éléments graphiques utilisé dans le tp d'animation a été créé
                              par
                              moi-même à l'aide
                              d'Adobe
                              Illustrator et Photoshop. Les fichiers .psd et .ai avec lesquels j'ai
                              travaillé sont disponibles
                              dans le repo GIT dans mon dossier images. Il n'y a que le fond étoilé
                              que j'ai
                              sauvegardé
                              directement en .jpeg, je n'ai pas son fichier Photoshop.</p>
                        <h3>Élément extérieur</h3>
                        <p>J'ai utilisé le plugin JQuery fireworks pour créer l'animation des feux
                              d'artifice à la fin de
                              l'animation. Source disponible <a href="https://www.jqueryscript.net/animation/Realistic-Fireworks-Animations-Using-jQuery-And-Canvas-fireworks-js.html">ici</a>.
                        </p>
                  </div>
            </div>
      </div>
</section>

<section class="section2">
      <div class="losange">
            <h2>L'heure est grave!</h2>
      </div>
      <div class="message conteneur">
            <div class="star"></div>
            <h3>À tous les habitants de la ville de Phort</h3>
            <p>Comme vous le savez tous, les dernières semaines n’ont été faciles pour personne. En effet,
                  la destruction de Phrak a eu un impact considérable sur notre quotidien. En plus du vol
                  massif d’anti-anxiolytique, de la pénurie de masques, de désinfectant et de produits
                  sanitaires en tout genre, créée par la contamination de l’explosion de leur fournaise
                  quantique, notre population globale s’est vu atteindre des sommets que nous n’aurions
                  jamais cru imaginables.</p>
            <p>Malheureusement, la structure de la ville n’est pas en mesure de supporter cette surcharge.
                  Nous apprécions évidemment nos voisins Phrakéens et souhaitons leur venir en aide au
                  maximum, sans pour autant détruire notre belle Phrak. Il a donc fallu trouver une
                  solution. Après plus de 78 ans d’hibernation, le Haut Conseil des 11 s’est donc réuni afin
                  d’établir un plan pour rééquilibrer la population. À la suite de longues discussions
                  (agrémentées de larmes et de poings levés!), le grand Pharto 1er, chef suprême du Haut
                  conseil a déclaré être venu à un consensus : Le Grand Tournoi – La mort éternelle. </p>
            <p>De plus en plus d’habitants nous ont mentionnés leur lassitude pour la vie et nous ont fait
                  part de leur souhait d’obtenir le droit à la mort. Le Grand Tournoi saura donc répondre à
                  leurs attentes. </p>
            <p>Son déroulement sera très simple. Chaque aïeul aura la possibilité, en tout temps, d’invoquer
                  son droit au tournoi. Ainsi, 99 autres participants seront sélectionnés aléatoirement pour
                  participer avec celui-ci. Les combats à mort « un contre un » se dérouleront dans l’arène
                  centrale de la ville. Les combattants seront d’abord séparés en deux groupes de 50. Les
                  vainqueurs s’affronteront de tour en tour jusqu’à ce qu’il ne reste qu’un seul combattant
                  par groupe. La grande finale du tournoi aura lieu ensuite entre les deux champions afin de
                  proclamer le grand vainqueur du Grand Tounoi – La mort éternelle!</p>
            <p>La victoire au tournoi permettra au champion de voir son rêve le plus cher se réaliser en
                  quittant enfin la vie vers la mort éternelle tant convoitée. Nous espérons ainsi voir
                  diminuer significativement, dans un délai raisonnable, la population de notre belle Phrak.
                  Pour vous inscrire au tournoi, veuillez suivre les indications ci-dessous! </p>
            <a href="signin.php" class="btn">S'inscrire</a>
      </div>
</section>
</main>

<?php
require 'includes/elements/footer.php';
?>
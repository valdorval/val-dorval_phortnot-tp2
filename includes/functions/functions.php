<?php
$errors = null;
$successMessage = null;
$regex = '/^(\+1|001)?\(?([0-9]{3})\)?([ .-]?)([0-9]{3})([ .-]?)([0-9]{4})/';
$titre_html = [
        '/index.php' => 'Phortnot - Accueil',
        '/blog.php' => 'Phortnot - Le blogue',
        '/boutique.php' => 'Phortnot - La boutique',
        '/champion.php' => 'Phortnot - Les champions du tournoi',
        '/logout.php' => 'Phortnot - Déconnexion',
        '/profil.php' => 'Phortnot - Votre profil',
        '/signin.php' => 'Phortnot - Connexion et inscription au tournoi Phortnot',
        '/stat.php' => 'Phortnot - Statistiques des champions'
];

function title($var)
{
        foreach ($var as $index => $value) {
                if ($index == htmlentities($_SERVER['PHP_SELF'])) {
                        return $value;
                }
        }
}


/**
 * Validate
 */

function form_values($nom)
{
        echo (isset($_POST[$nom]) ? $_POST[$nom] : "");
}

function validate_email($courriel)
{
        return filter_var($courriel, FILTER_VALIDATE_EMAIL);
}

if ($_SERVER["REQUEST_METHOD"] == "POST"  && isset($_POST['submit'])) {
        $form = validate_contact();
        if (count($form) > 1) {
                $errors = $form;
        }
        return $errors;
}

function validate_contact()
{
        $errors = [];
        $errors['last_name'] = (empty($_POST['last_name']) ? '- Le nom de famille ne peut pas être vide' : '');
        $errors['first_name'] = (empty($_POST['first_name']) ? '- Le prénom ne peut pas être vide' : '');
        $errors['birth'] = (empty($_POST['birth']) ? '- La date de naissance  ne peut pas être vide' : '');
        $errors['user_name'] = (empty($_POST['user_name']) ? '- Le nom d\'utilisateur ne peut pas être vide' : '');
        $errors['email'] = (empty($_POST['email']) || !validate_email($_POST['email']) ? '- L\'adresse courriel n\'est pas valide' : '');
        $errors['phone'] = (empty($_POST['phone']) || !preg_match($regex, $_POST['phone']) ? '- Le numéro de téléphone n\'est pas invalide' : '');

        if (empty($_POST['password'])) {
                $errors['password'] = '- Le mot de passe ne peut pas être vide';
        } elseif ($_POST['password'] != $_POST['password_confirm']) {
                $errors['password'] = '- Le mot de passe n\'est pas le même';
        } else {
                $errors['password'] = '';
        }

        if (count($errors) >= 1) {
                return $errors;
        }
}

function encrypt_pass($pass)
{
        $hash_pwd = password_hash($pass, PASSWORD_BCRYPT);
        return $hash_pwd;
}

/**
 * Format
 */

function format_phone($data)
{
        $verify_regex = '~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~';
        if (preg_match($verify_regex, $data, $matches)) {
                $result = '(' . $matches[1] . ') ' . $matches[2] . '-' . $matches[3];
                return $result;
        }
}

function phone_replace($data)
{
        $phone = preg_replace('/\D+/', '', $data);
        return $phone;
}

/**
 * Edit
 */

function edit_phone($value, $id)
{
        $db = connect();
        try {
                $data =  $db->prepare('UPDATE profil SET  phone = ? WHERE id = ?');
                $data->execute([$value, $id]);
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
}

function edit_email($value, $id)
{
        $db = connect();
        try {
                $data =  $db->prepare('UPDATE profil SET  email = ? WHERE id = ?');
                $data->execute([$value, $id]);
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
}

function edit_user($value, $id)
{
        $db = connect();
        try {
                $data =  $db->prepare('UPDATE profil SET username = ? WHERE id = ?');
                $data->execute([$value, $id]);
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
}

function edit_pass($value, $id)
{
        $db = connect();
        try {
                $data =  $db->prepare('UPDATE profil SET  pwd = ? WHERE id = ?');
                $data->execute([$value, $id]);
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
}

/**
 * DB
 */

function tournoi_info()
{
        $conn = connect();
        try {
                $req = $conn->query('SELECT p.username, p.id_image, t.win, t.lose, t.champion, t.death
                FROM profil AS p
                LEFT OUTER JOIN tournoi AS t ON p.id = t.id_profil ORDER BY t.win DESC');
                return $req;
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
        $conn = null;
        exit();
}

function connect_by_id($id)
{
        $conn = connect();
        try {
                $requete = $conn->prepare("SELECT * FROM profil WHERE id = ?");
                $requete->execute([$id]);
                return $requete;
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
        $conn = null;
        exit();
}

/**
 * Connecteur db
 */

function connect()
{
        $serveur = "localhost"; //127.0.0.1
        $dbname = "phortnot";
        $user = "root";
        $password = "";
        $options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
        ];


        try {
                $pdo = new PDO("mysql:host=$serveur;dbname=$dbname", $user, $password, $options);
                return $pdo;
        } catch (PDOException $e) {
                throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
        return $pdo;
}

/* 
À mettre en forme pour la suite 
*/

// if ($_SERVER["REQUEST_METHOD"] == "POST") {
//         form_routing();
// }

// function form_routing()
// {
//         if (isset($_POST["contact"])) {
//                 $errors = validate_form($_POST);
//         }

//         if (isset($_POST["submit"])) {
//                 if (count(validate_contact($_POST)) < 1) {
//                         $register = register_user($_POST);
//                         header('Location: profil.php?message=Votre compte à bien été créé');
//                 }
//         }

//         if (isset($_POST["login"])) {
//                 if (count(validate_form($_POST)) < 1) {
//                         try {
//                                 $login = login($_POST);
//                                 return $login;
//                         } catch (Exception $e) {
//                                 echo $e->getMessage();
//                         }
//                 }
//         }
// }


function dd($data)
{
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
        die;
}

function d($data)
{
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
}

function debug($variable)
{
        echo '<pre>' . var_dump($variable) . '</pre>';
}

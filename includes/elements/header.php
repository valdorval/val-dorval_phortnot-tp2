<?php
include 'includes/functions/functions.php';
include 'includes/functions/session.php';
?>

<!doctype html>
<html class="no-js" lang="">

<head>
      <meta charset="utf-8">
      <title><?php echo title($titre_html); ?></title>
      <meta name="description" content="Page d'accueil du tournoi de Phortnot">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="manifest" href="site.webmanifest">
      <link rel="apple-touch-icon" href="icon.png">
      <!-- Place favicon.ico in the root directory -->
      <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Titillium+Web:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
      <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.min.css" integrity="sha256-6hqHMqXTVEds1R8HgKisLm3l/doneQs+rS1a5NLmwwo=" crossorigin="anonymous" />
      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/anim.css">

      <meta name="theme-color" content="#fafafa">
</head>

<body>
      <header class="header">
            <div class="header-background flex">
                  <div class="logo animated__animated animate__bouceInLeft"></div>
            </div>

            <div class="sidebar">
                  <?php if (isset($_SESSION['auth'])) : ?>
                        <a href="logout.php" class="btn-menu">Déconnexion</a>
                  <?php else : ?>
                        <a href="signin.php" class="btn-menu">Se connecter</a>
                  <?php endif; ?>
                  <ul class="nav-menu">
                        <li><a href="index.php">Accueil</a></li>
                        <li><a href="champion.php">Champions</a></li>
                        <li><a href="stat.php">Statistiques</a></li>
                        <li><a href="boutique.php">Boutique</a></li>
                        <li><a href="blog.php">Blogue</a></li>
                        <li><a href="profil.php">Profil</a></li>
                  </ul>
                  <div class="sidebarLogo"></div>
                  <button class="sidebarBtn">
                        <span></span>
                  </button>
            </div>
      </header>

      <main>
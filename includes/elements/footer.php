<footer class="footer">
            <span>Site créé par Valérie Dorval</span>
      </footer>

      <script src="js/vendor/modernizr-3.8.0.min.js"></script>
      <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
      <script src="js/plugins.js"></script>
      <script src="js/jquery.fireworks.js"></script>
      <script src="js/app.js"></script>

      <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
      <script>
            window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
            ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
      </script>
      <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>